#!/usr/bin/env python
# -*- coding: utf-8 -*-

class RuntasticToSTRAVAUploader(object):
    """
    Automated activity uploader from Runtastic to STRAVA.
    """

    def __init__(self, homeDirPath, runtasticAuthInfoFilePath):
        """
        homeDirPath -- Script home directory path. Used for config and data storage.
        runtasticAuthInfoFilePath -- Runtastic authentication info file path.
            Requred to be a JSON with the followin contents:
            {
                "email": "my@mail.com",
                "password": "MyPassword"
            }
        """

        self._configFileName    = 'config.json'
        self._homeDirPath       = homeDirPath
        self._runtasticAuthInfo = self._loadRuntasticAuthInfo(runtasticAuthInfoFilePath)
        self._config            = self._loadConfig()

    def autoUpdate(self):
        """
        Automatically upload any new activity from Runtastic to STRAVA.
        Saves update date to config to avoid downloading any old activities in the future.
        """

        from os import getcwd, chdir

        cwd = getcwd()
        try:
            chdir(self._homeDirPath)

            anyNewActivities = self._downloadFromRuntastic()
            if (not anyNewActivities):
                return
            self._updateLastDate()
            packageName = self._findDownloadedRuntasticPackage()
            extractionDirName = self._unzipAndRemoveRuntasticPackage(packageName)
            self._uploadToSTRAVA(extractionDirName)
        finally:
            chdir(cwd)

    def _downloadFromRuntastic(self):
        """
        Download activities from Runtastic to home directory.
        """

        from subprocess import check_output, CalledProcessError, STDOUT

        try:
            check_output([
                'runtastic',
                    '-email', self._runtasticAuthInfo['email'],
                    '-password', self._runtasticAuthInfo['password'],
                    '-format', 'tcx',
                    '-laterThan', self._config['lastDate']],
                stderr = STDOUT)
            return True
        except CalledProcessError as e:
            if ('There are no activities to backup' in e.output):
                return False
            else:
                raise

    def _updateLastDate(self):
        """
        Save today as the last update date to config.
        """

        self._config['lastDate'] = self._getRFC3339UTCDate()
        self._saveConfig(self._config)

    def _findDownloadedRuntasticPackage(self):
        """
        Get the path to Runtastic Archiver downloaded package.
        Fetches the newest package path in home directory.
        """

        from os import listdir
        from re import match

        runtasticPackages = [
            filename
            for filename
            in listdir('./')
            if (match('Runtastic \d{4}-\d{2}-\d{2} \d{2}.\d{2}.\d{2}.zip', filename))]

        return sorted(runtasticPackages, reverse= True)[0]

    def _unzipAndRemoveRuntasticPackage(self, packageName):
        """
        Unzips the package downloaded by Runtastic Archiver and removes the package.
        """

        from zipfile import ZipFile
        from re import sub
        from os import mkdir, remove

        extractionDirName = sub('\.zip$', '', packageName)

        mkdir(extractionDirName)

        with ZipFile(packageName, 'r') as package:
            package.extractall(extractionDirName)

        remove(packageName)

        return extractionDirName

    def _listExtractedRuntasticPackage(self, extractionDirName):
        """
        Lists activity files in the extracted Runtastic Archiver folder.
        """

        from os import listdir
        from os.path import join

        return [
            join(extractionDirName, name)
            for name
            in listdir(extractionDirName)
            if (name.endswith('.tcx'))]

    def _uploadToSTRAVA(self, extractionDirName):
        """
        Uploads all activity files from the directory to STRAVA.
        """

        from stravaup import Run

        activities = self._listExtractedRuntasticPackage(extractionDirName)
        Run(['--no-popup'] + activities)

    def _loadConfig(self):
        """
        Loads the tool config.
        """

        from os import makedirs
        from os.path import exists
        from json import load

        if (exists(self._homeDirPath) and
            exists(self._configFilePath)):
            return load(open(self._configFilePath))
        else:
            if (not exists(self._homeDirPath)):
                makedirs(self._homeDirPath)
            return {
                'lastDate': '1970-01-01T00:00:00Z'
                }

    def _saveConfig(self, config):
        """
        Saves the tool config.
        """

        from json import dump

        dump(config, open(self._configFileName, 'w'))

    @property
    def _configFilePath(self):
        """
        Path to configuration file.
        """

        from os.path import join

        return join(self._homeDirPath, self._configFileName)

    @staticmethod
    def _getRFC3339UTCDate():
        """
        Current date in RFC 3339 UTC (Zulu) format.
        """

        from datetime import datetime

        return datetime.utcnow().isoformat('T') + 'Z'

    @staticmethod
    def _loadRuntasticAuthInfo(runtasticAuthInfoFilePath):
        """
        Loads Runtastic authentication info from the file.
        """

        from json import load

        return load(open(runtasticAuthInfoFilePath))

def parseCommandline(args = None):
    from argparse import ArgumentParser

    parser = ArgumentParser()

    parser.add_argument(
        '--homeDirPath',
        default = '~/.RuntasticToStrava',
        help    = 'Home directory path used for downloaded activities and configuration storage. Tilda (~) is expaded to user home directory.')
    parser.add_argument(
        '--runtasticAuthInfoFilePath',
        default = '~/.Runtastic.json',
        help    = 'Runtastic authentication JSON file. Tilda (~) is expaded to user home directory.')

    return parser.parse_args(args)

def main():
    from os.path import expanduser

    params = parseCommandline()
    uploader = RuntasticToSTRAVAUploader(
        homeDirPath               = expanduser(params.homeDirPath),
        runtasticAuthInfoFilePath = expanduser(params.runtasticAuthInfoFilePath))
    uploader.autoUpdate()

if (__name__ == '__main__'):
    main()
