# Runtastic to STRAVA automatic upload script
Automatically uploads all new activities from Runtastic to STRAVA. Script uses Python 2.7.

## Prerequisites
Following needs to be present in the script folder, PATH or PYTHONPATH or installed through tools like `pip`:
* Modified [Runtastic Archiver](https://github.com/maranov/runtastic), [download for Windows](https://gitlab.com/maranov/RuntasticToSTRAVA/uploads/b0a5fc7e0193547f796d911aa14197ae/runtastic.zip).
* [stravacli](https://github.com/dlenski/stravacli).
    * Requires [stravalib](https://github.com/hozn/stravalib).

stravacli expects STRAVA authentication to be present in the `~/.stravacli` file, see [stravacli Readme](https://github.com/dlenski/stravacli#application-authorization).

## Usage
See help:
```
python RuntasticToSTRAVA.py -h
```

Runtastic authentication info must be stored in a JSON file with the following structure:
``` json
{
    "email": "put.your.mail@here.com",
    "password": "PutYourPasswordHere"
}
```
